California Pools is one of the largest pool builders in the country and brings award-winning custom pools to Orange County. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 731 E Ball Road, #104, Anaheim, CA 92805, USA

Phone: 714-724-8887

Website: https://californiapools.com/locations/orange-county-north
